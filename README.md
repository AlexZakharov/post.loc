Test project: 'Post'
============================
#### PHP: 5.5.9+
#### Framework: Yii 2.*
#### Database: Sqlite +
#### Specification: none

INSTALLATION
------------
#### Clone project
~~~
git clone https://{your account on bitbucket}@bitbucket.org/AlexZakharov/post.loc.git
~~~

#### Composer. In project folder
~~~
composer install
~~~

#### Go to project folder and check migration and fixture
~~~
bin/yii migrate

bin/yii fixture/load "*"
~~~

DIRECTORY STRUCTURE
-------------------

      application/assets/             contains assets definition
      application/base/               contains base classes
      application/commands/           contains console commands (controllers)
      application/component/          contains bootstrap component
      application/services/           contains basic project components
      application/controllers/        contains web controller classes
      application/enums/              contains enum classes
      application/mail/               contains view files for e-mails
      application/models/             contains model classes
      application/modules/            contains web modules
      application/tests/              contains various tests for the basic application
      application/views/              contains view files for the Web application
      bin/                            contains bash scripts
      config/                         contains application configurations
      config/local                    contains local application configurations
      runtime/                        contains files generated during runtime
      temp/                           contains pdf tickets
      vendor/                         contains dependent 3rd-party packages
      web/                            contains the entry script and Web resources
      web/assets                      contains web assets
      web/css                         contains styles folder
      web/js                          contains java-script folder