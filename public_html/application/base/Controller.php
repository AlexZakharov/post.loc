<?php
namespace app\base;

use \Yii;
use \yii\db\ActiveRecord;
use \yii\web\Response;
use \yii\helpers\Json;
use \yii\widgets\ActiveForm;
use \yii\web\NotFoundHttpException;
use \app\enums\UserType;

/**
 * Application base controller.
 * @package app\base\Controller
 * @version 1.0
 * @copyright (c) 2016-2017 KFOSOFT Team <kfosoftware@gmail.com>
 */
class Controller extends \yii\web\Controller
{
    protected function filterAssets()
    {
        Yii::$app->assetManager->bundles = [
            'yii\bootstrap\BootstrapPluginAsset' => false,
            'yii\bootstrap\BootstrapAsset' => false,
            'yii\web\JqueryAsset' => false,
            'yii\web\YiiAsset' => false,
            'yii\widgets\PjaxAsset' => false
        ];
    }

    /**
     * Perform ajax validation.
     * @param \app\base\ActiveRecord $model model for validate.
     * @return array the error message array indexed by the attribute IDs.
     * @author Cyril Turkevich
     */
    protected function performAjaxValidation(&$model)
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            echo Json::encode(ActiveForm::validate($model));
            Yii::$app->end();
        }
    }

    /**
     * Finds the Attractor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string|ActiveRecord $modelClass model or model class.
     * @param integer $id model id.
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @author Cyril Turkevich
     */
    protected function findModel($modelClass, $id)
    {
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}