<?php

namespace app\base;


/**
 * Base Class Model
 *
 * @package app\models\base
 * @version 1.0
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class Model extends \app\base\ActiveRecord
{
    /**
     * Universal method for updating model field
     * @param $field string field name
     * @param $value mixed field value
     * @return bool Save result
     */
    public function updateField($field, $value)
    {
        if (isset($this->{$field})) {
            $this->{$field} = $value;

            return $this->save(false);
        }

        return false;
    }
}