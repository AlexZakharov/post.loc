<?php

namespace app\enums;

use Yii;
use app\enums\base\Enum;

/**
 * Class UserStatus
 * @package app\enums
 * @version 1.0
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class UserStatus extends Enum
{
    const ROLE_ADMIN = 1;
    const ROLE_USER  = 2;


    /**
     * List data.
     * @return array|null data
     */
    public static function listData()
    {
        return [
          self::ROLE_ADMIN   => Yii::t('app', 'Admin'),
          self::ROLE_USER => Yii::t('app', 'User'),
        ];
    }

}
