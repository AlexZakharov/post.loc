<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;
/**
 * create table post
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class m170212_200818_post extends Migration
{
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'created_at' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'updated_at' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('post');
    }
}
