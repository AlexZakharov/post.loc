<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;
/**
 * create table post_description
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class m170212_201225_post_description extends Migration
{
    public function safeUp()
    {
        $this->createTable('post_description', [
            'id' => Schema::TYPE_PK,
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL REFERENCES post(id)',
            'language_code' => Schema::TYPE_STRING . '(3) NOT NULL DEFAULT "us"',
            'text' => Schema::TYPE_TEXT
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('post_description');
    }
}
