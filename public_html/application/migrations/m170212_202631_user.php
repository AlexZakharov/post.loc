<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;
/**
 * create table user
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class m170212_202631_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'status' => Schema::TYPE_SMALLINT . '(1) DEFAULT NULL',
            'created_at' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'updated_at' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'password' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'authKey' => Schema::TYPE_STRING,
            'accessToken' => Schema::TYPE_STRING
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('user');
    }
}
