<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;

/**
 * create table user
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class m170212_203147_user_profile extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_profile', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL REFERENCES user(id)',
            'first_name' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'last_name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('user_profile');
    }
}
