<?php

namespace app\models;

use Yii;
use app\models\base\LoginForm as BaseLoginForm;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends BaseLoginForm
{

}