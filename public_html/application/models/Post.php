<?php

namespace app\models;

use Yii;
use app\models\base\Post as BasePost;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PostDescription[] $postDescriptions
 */
class Post extends BasePost
{
    /**
     * @var string
     */
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            ['verifyCode', 'captcha'],
            [['created_by', 'updated_by'], 'integer'],
            [['title', 'created_at', 'updated_at'], 'string', 'max' => 255],
        ];
    }

}
