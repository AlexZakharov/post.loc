<?php

namespace app\models;

use Yii;
use app\models\base\PostDescription as BasePostDescription;

/**
 * This is the model class for table "post_description".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $language_code
 * @property string $text
 *
 * @property Post $post
 */
class PostDescription extends BasePostDescription
{

}
