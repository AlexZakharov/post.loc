<?php

namespace app\models;

use Yii;
use app\models\base\User as BaseUser;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 *
 * @property UserProfile[] $userProfiles
 */
class User extends BaseUser
{

}
