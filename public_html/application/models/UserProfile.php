<?php

namespace app\models;

use Yii;
use app\models\base\UserProfile as BaseUserProfile;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 *
 * @property User $user
 */
class UserProfile extends BaseUserProfile
{

}
