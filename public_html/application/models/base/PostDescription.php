<?php

namespace app\models\base;

use app\base\Model;
use Yii;

/**
 * This is the model class for table "post_description".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $language_code
 * @property string $text
 *
 * @property Post $post
 */
class PostDescription extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'text', 'language_code'], 'required'],
            [['post_id'], 'integer'],
            [['text'], 'string'],
            [['language_code'], 'string', 'max' => 3],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'language_code' => 'Language Code',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
