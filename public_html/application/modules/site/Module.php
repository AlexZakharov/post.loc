<?php

namespace app\modules\site;

use \Yii;
use \Yii\helpers\Url;
use \yii\filters\AccessControl;
use \app\interfaces\Menu as MenuInterface;
use \app\enums\Language;
use \app\enums\RosterItemType;

/**
 * Web module for administration panel.
 * @package app\modules\admin
 * @version 1.0
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class Module extends \yii\base\Module
{
    /** @var string $controllerNamespace controller namespace */
    public $controllerNamespace = 'app\modules\site\controllers';

}
