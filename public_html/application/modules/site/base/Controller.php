<?php
namespace app\modules\admin\base;

/**
 * Base admin controller.
 * @package app\modules\admin\base
 * @version 1.0
 * @copyright (c) 2016-2017 KFOSOFT Team <kfosoftware@gmail.com>
 */
class Controller extends \app\base\Controller
{
    /** @var string $layout admin default layout. */
    public $layout = 'main.php';

}