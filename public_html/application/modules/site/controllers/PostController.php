<?php

namespace app\modules\site\controllers;

use app\base\Controller;
use app\models\PostDescription;
use Yii;
use app\models\Post;
use app\models\base\Post as BasePost;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'mdm\captcha\CaptchaAction',
                'level' => 3, // avaliable level are 1,2,3 :D
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BasePost::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = \app\modules\site\models\Post::findOne(['id'=> $id]);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            $model = new Post();
        }
        /** @var \app\models\base\Post $model */
        $model = new BasePost();
        /** @var \app\models\PostDescription $modelDescription */
        $modelDescription = new PostDescription();

        if($model->load(Yii::$app->request->post()))
        {
            /** Start transaction*/
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $model->created_at = time();
                $model->created_by = Yii::$app->user->id;

                if($model->save(false)){
                    $modelDescription->load(Yii::$app->request->post());
                    $modelDescription->post_id = $model->id;
                    if($modelDescription->save(false)){
                        /** Commit Transaction */
                        $transaction->commit();
                        /** redirect to view */
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }catch (Exception $e) {
                $transaction->rollBack();
            }
        }else {
            return $this->render('create', [
                'model' => $model,
                'modelDescription' => $modelDescription,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);
        $modelDescription = PostDescription::findOne(['post_id' => $id]);

        if($model->load(Yii::$app->request->post()))
        {
            /** Start transaction*/
            $transaction = \Yii::$app->db->beginTransaction();
            try {

                $model->created_at = time();
                $model->created_by = Yii::$app->user->id;

                if($model->save(false)){
                    $modelDescription->load(Yii::$app->request->post());
                    $modelDescription->post_id = $model->id;
                    if($modelDescription->save(false)){
                        /** Commit Transaction */
                        $transaction->commit();
                        /** redirect to view */
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }catch (Exception $e) {
                $transaction->rollBack();
            }
        }else {
            return $this->render('update', [
                'model' => $model,
                'modelDescription' => $modelDescription,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['index']);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BasePost::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
