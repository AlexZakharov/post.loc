<?php

namespace app\modules\site\models;

use app\models\base\PostDescription;
use Yii;
use app\models\Post as PostModel;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $text
 * @property string $language_code
 *
 * @property PostDescription[] $postDescriptions
 */
class Post extends PostModel
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by'], 'integer'],
            [['title', 'created_at', 'updated_at', 'language_code', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'language_code'  => Yii::t('app', 'Language Code'),
            'text'      => Yii::t('app', 'Text'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getLanguage_code()
    {
        $text = PostDescription::findOne(['id' => $this->id]);
        return $text->language_code;
    }

    /**
     * @inheritdoc
     */
    public function getText()
    {
        $text = PostDescription::findOne(['id' => $this->id]);
        return $text->text;
    }

}
