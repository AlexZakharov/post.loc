<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $modelDescription app\models\PostDescription */

$this->title = 'Create Post';
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if(Yii::$app->user->isGuest){
        echo $this->render('partials/_formGuest', [
            'model' => $model,
            'modelDescription' => $modelDescription,
        ]);
    }else{
        echo $this->render('partials/_form', [
            'model' => $model,
            'modelDescription' => $modelDescription,
        ]);
    }
    ?>

</div>
