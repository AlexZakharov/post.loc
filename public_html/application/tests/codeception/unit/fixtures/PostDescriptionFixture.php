<?php
namespace app\tests\codeception\unit\fixtures;

use app\models\base\PostDescription as PostDescriptionModel;
use app\tests\codeception\unit\fixtures\base\BaseActiveFixture;

/**
 * Class PostDescriptionFixture
 * @package app\tests\codeception\unit\fixtures
 * @version 1.0
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class PostDescriptionFixture extends BaseActiveFixture
{
    /** @var string */
    public $modelClass = PostDescriptionModel::class;
    /** @var array */
    public $depends = [PostFixture::class];
}