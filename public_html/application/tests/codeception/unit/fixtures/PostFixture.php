<?php
namespace app\tests\codeception\unit\fixtures;

use app\models\base\Post as PostModel;
use app\tests\codeception\unit\fixtures\base\BaseActiveFixture;

/**
 * Class PostFixture
 * @package app\tests\codeception\unit\fixtures
 * @version 1.0
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class PostFixture extends BaseActiveFixture
{
    /** @var string */
    public $modelClass = PostModel::class;
}