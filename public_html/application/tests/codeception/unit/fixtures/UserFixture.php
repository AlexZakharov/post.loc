<?php
namespace app\tests\codeception\unit\fixtures;

use app\models\base\User as UserModel;
use app\tests\codeception\unit\fixtures\base\BaseActiveFixture;

/**
 * Class UserFixture
 * @package app\tests\codeception\unit\fixtures
 * @version 1.0
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class UserFixture extends BaseActiveFixture
{
    /** @var string */
    public $modelClass = UserModel::class;
}