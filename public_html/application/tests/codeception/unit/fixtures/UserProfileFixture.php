<?php
namespace app\tests\codeception\unit\fixtures;

use app\models\base\UserProfile as UserProfileModel;
use app\tests\codeception\unit\fixtures\base\BaseActiveFixture;

/**
 * Class UserProfileFixture
 * @package app\tests\codeception\unit\fixtures
 * @version 1.0
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class UserProfileFixture extends BaseActiveFixture
{
    /** @var string */
    public $modelClass = UserProfileModel::class;
    /** @var array */
    public $depends = [UserFixture::class];
}