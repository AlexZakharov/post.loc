<?php

namespace app\tests\codeception\unit\fixtures\base;

use \yii\test\ActiveFixture;
use \app\exceptions\FileNotFoundException;
use yii\web\NotFoundHttpException;

/**
 * Class BaseActiveFixture
 * @package app\tests\codeception\unit\fixtures\base
 * @version 1.1
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 * @copyright (c) 2014-2015 KFOSoftware Team <kfosoftware@gmail.com>
 */
class BaseActiveFixture extends ActiveFixture
{
    /**
     * This method is different by the presence of the parent checks the register file name
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function getData()
    {
        if ($this->dataFile === null) {
            $class = new \ReflectionClass($this);
            $fileName = $this->getTableSchema()->fullName . '.php';
            $dirName = dirname($class->getFileName()) . '/data/';
            $dataFile = $dirName . $fileName;

            if (!file_exists($dataFile)) {
                $lowerCaseFileName = strtolower($fileName);
                $dataFile = dirname($class->getFileName()) . '/data/' . $lowerCaseFileName;

                if (!file_exists($dataFile)) {
                    throw new NotFoundHttpException($dirName, [$fileName, $lowerCaseFileName]);   //Exception
                }
            }
            return require($dataFile);
        } else {
            return parent::getData();
        }
    }
}