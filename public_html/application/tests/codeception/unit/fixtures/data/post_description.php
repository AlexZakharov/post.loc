<?php
return [
    [
        'id' => 1,
        'post_id' => 1,
        'language_code' => 'us',
        'text' => 'This is first text.'
    ],
    [
        'id' => 2,
        'post_id' => 2,
        'language_code' => 'ru',
        'text' => 'This is second text.'
    ]
];