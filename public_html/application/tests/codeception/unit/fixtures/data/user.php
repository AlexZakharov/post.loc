<?php
return [
    [
        'id' => 1,
        'username' => 'Admin',
        'email' => 'admin@gmail.com',
        'status' => \app\enums\UserStatus::ROLE_ADMIN,
        'created_at' => time(),
        'updated_at' => time(),
        'password' => '123123',
        'authKey' => 'test100key',
        'accessToken' => '100-token',
    ],
    [
        'id' => 2,
        'username' => 'User',
        'email' => 'demo@gmail.com',
        'status' => \app\enums\UserStatus::ROLE_USER,
        'created_at' => time(),
        'updated_at' => time(),
        'password' => '123123',
        'authKey' => 'test101key',
        'accessToken' => '100-token',
    ],
];