<?php
return [
    [
        'id' => 1,
        'user_id' => 1,
        'first_name' => 'Admin',
        'last_name' => 'Admin'
    ],
    [
        'id' => 2,
        'user_id' => 2,
        'first_name' => 'User',
        'last_name' => 'Demo'
    ],
];