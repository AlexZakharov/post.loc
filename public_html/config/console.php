<?php
use yii\helpers\ArrayHelper;

Yii::setAlias('@tests', dirname(__DIR__) . '/tests/codeception');

/** @var string $localConfigPath Path to local config */
$localConfigPath = __DIR__ . '/local/console.php';

Yii::setAlias('@config', __DIR__);
return ArrayHelper::merge([
    'id' => 'basic-console',
    'basePath'              => dirname(__DIR__) . '/application',
    'runtimePath'           => dirname(__DIR__) . '/runtime',
    'bootstrap' => ['log'],
    'controllerNamespace'   => 'app\commands',
    'controllerMap'         => [
        'migrate' => [
            'class'          => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration',
        ],
        'fixture' => [
            'class'          => 'yii\console\controllers\FixtureController',
            'namespace'      => 'app\tests\codeception\unit\fixtures',
            'globalFixtures' => [],
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => require(__DIR__ . '/params.php'),
], is_file($localConfigPath) ? require($localConfigPath) : []);
