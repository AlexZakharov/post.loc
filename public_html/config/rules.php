<?php
return [
    [
        'pattern' => '/',
        'route' => 'site/user/index'
    ],
    [
        'pattern'   => '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>',
        'route'     => '<module>/<controller>/<action>',
    ],
    [
        'pattern'   => '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>',
        'route'     => '<module>/<controller>/<action>',
    ],
    [
        'pattern'   => '<controller:\w+>/<action:[\w-]+>/<id:\d+>',
        'route'     => '<controller>/<action>',
    ],
    [
        'pattern'   => '<controller:\w+>/<action:[\w-]+>',
        'route'     => '<controller>/<action>',
    ],
    [
        'pattern'   => '<controller:\w+>/<id:\d+>',
        'route'     => '<controller>/view',
    ],
];