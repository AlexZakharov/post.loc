<?php
use yii\helpers\ArrayHelper;

$root =dirname(__DIR__);
Yii::setAlias('@config', __DIR__);
Yii::setAlias('@commonViews', $root . '/application/views/common');
Yii::setAlias('@widgets', $root . '/application/widgets');

/** @var string $localConfigPath Path to local config */
$localConfigPath = __DIR__ . '/local/web.php';

$config = [
    'id' => 'basic',
    'basePath' => $root . '/application',
    'name' => 'Post',
    'bootstrap' => [
        'log',
        'urlManager',
    ],
    'vendorPath' => $root . '/vendor',
    'runtimePath' => $root . '/runtime',
    // set target language to be English
    'language' => 'en-US',
    // set source language to be English
    'sourceLanguage' => 'en-US',
    'timeZone' => 'Europe/Kiev',
    'modules' => [
        'site' => [
            'class' => '\app\modules\site\Module',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'eKiydNqoE9EizKgz7ZNvDNz70pzqjhrn',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'rules' => require __DIR__ . DIRECTORY_SEPARATOR . 'rules.php',
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => require(__DIR__ . '/params.php'),
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return ArrayHelper::merge($config, is_file($localConfigPath) ? require($localConfigPath) : []);
